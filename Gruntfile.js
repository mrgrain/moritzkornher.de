const sass = require("node-sass");

module.exports = function (grunt) {
  // Configure grunt
  grunt.initConfig({
    pkg: grunt.file.readJSON("package.json"),

    path: {
      src: "source",
      target: "build",
      assets: "/assets",
      temp: "tmp",
      layouts: "<%= path.src %>/layouts",
      pages: "<%= path.src %>/pages",
      scss: "<%= path.src %>/scss",
      js: "<%= path.src %>/js",
      images: "<%= path.src %>/images",
      files: "<%= path.src %>/files",
      fonts: "<%= path.src %>/fonts",
    },

    // File hashes
    smartrev: {
      options: {
        cwd: "<%= path.target %>",
        noRename: ["index.html", "files/**", "fonts/**"],
      },
      build: {
        src: ["index.html"],
      },
    },

    // HTML
    assemble: {
      options: {
        flatten: true,
        data: ["./package.json"],
        layoutdir: "<%= path.layouts %>",
        layout: "default.hbs",
        partials: ["<%= path.layouts %>/partials/**/*.hbs"],
        helpers: ["handlebars-helper-aggregate", "helper-even.js"],
      },
      dev: {
        options: {
          assets: "<%= path.target %>",
        },
        files: [
          {
            src: "<%= path.src %>/index.hbs",
            dest: "<%= path.target %>/index.html",
          },
          {
            expand: true,
            cwd: "<%= path.pages %>",
            src: "*.hbs",
            dest: "<%= path.temp %>/pages",
          },
          {
            expand: true,
            cwd: "<%= path.src %>/projects",
            src: "*.{hbs,md}",
            dest: "<%= path.temp %>/projects",
          },
        ],
      },
    },

    // SASS
    sass: {
      options: {
        implementation: sass,
      },
      dev: {
        files: [
          {
            expand: true,
            cwd: "<%= path.scss %>",
            src: "*.{scss,css}",
            dest: "<%= path.target %>/css",
            ext: ".css",
          },
        ],
      },
      build: {
        options: {
          outputStyle: "compressed",
        },
        files: "<%= sass.dev.files %>",
      },
    },

    // JavaScript
    uglify: {
      dev: {
        options: {
          beautify: true,
          mangle: false,
          compress: false,
          preserveComments: true,
        },
        src: "<%= path.js %>/**/*.js",
        dest: "<%= path.target %>/js/<%= pkg.name %>.js",
      },
      build: {
        options: {
          beautify: false,
          mangle: true,
          compress: {
            drop_console: true,
          },
          preserveComments: false,
          sourceMap: false,
        },
        src: "<%= uglify.dev.src %>",
        dest: "<%= uglify.dev.dest %>",
      },
    },

    // Files
    copy: {
      npmmodules: {
        files: [
          {
            src: "./node_modules/normalize.css/normalize.css",
            dest: "<%= path.scss %>/_normalize.scss",
          },
          {
            src: "./node_modules/zepto/dist/zepto.js",
            dest: "<%= path.js %>/_zepto.js",
          },
          {
            expand: true,
            cwd: "./node_modules/font-awesome/fonts/",
            src: "*",
            dest: "<%= path.fonts %>/font-awesome",
          },
          {
            expand: true,
            cwd: "./node_modules/font-awesome/scss/",
            src: "*.scss",
            dest: "<%= path.scss %>/font-awesome",
          },
        ],
      },
      images: {
        expand: true,
        cwd: "<%= path.images %>",
        src: ["**/*.{png,jpg,jpeg,gif,svg}"],
        dest: "<%= path.target %>/images",
      },
      favicon: {
        expand: true,
        cwd: "<%= path.src %>",
        src: [
          "android-chrome-192x192.png",
          "android-chrome-384x384.png",
          "apple-touch-icon.png",
          "browserconfig.xml",
          "favicon.ico",
          "favicon-16x16.png",
          "favicon-32x32.png",
          "mstile-150x150.png",
          "site.webmanifest",
        ],
        dest: "<%= path.target %>/",
      },
      files: {
        expand: true,
        cwd: "<%= path.files %>",
        src: ["**/*.*"],
        dest: "<%= path.target %>/files",
      },
      fonts: {
        expand: true,
        cwd: "<%= path.fonts %>",
        src: ["**/*.{eot,svg,ttf,woff,woff2}"],
        dest: "<%= path.target %>/fonts",
      },
    },

    // CLEAN
    clean: ["<%= path.temp %>"],

    // TESTING
    htmlhint: {
      options: {
        htmlhintrc: ".htmlhintrc",
        force: true,
      },
      dev: {
        src: ["<%= path.target %>/**/*.html"],
      },
    },
    jshint: {
      options: {
        jshintrc: ".jshintrc",
        force: true,
        reporter: require("jshint-stylish"),
      },
      dev: {
        src: ["<%= path.js %>/*.js", "!<%= path.js %>/_zepto.js"],
      },
    },

    // LIVE
    watch: {
      options: {
        spawn: false,
        livereload: true,
      },
      html: {
        files: [
          "<%= path.src %>/index.hbs",
          "<%= path.pages %>/**/*.{hbs,md}",
          "<%= path.src %>/projects/**/*.{hbs,md}",
          "<%= path.layouts %>/**",
        ],
        tasks: ["assemble:dev", "assemble:dev"],
      },
      scss: {
        files: ["<%= path.scss %>/*.scss"],
        tasks: ["sass:dev"],
      },
      js: {
        files: ["<%= path.js %>/*.js"],
        tasks: ["uglify:dev"],
      },
      images: {
        files: [
          "<%= path.images %>/**/*.{png,jpg,jpeg,gif,svg}",
          "<%= path.src %>/favicon.ico",
        ],
        tasks: ["newer:copy:images", "newer:copy:favicon"],
      },
      files: {
        files: ["<%= path.files %>/**/*.*"],
        tasks: ["newer:copy:files"],
      },
      fonts: {
        files: ["<%= path.fonts %>/**/*.*"],
        tasks: ["newer:copy:fonts"],
      },
    },
  });

  // load default tasks
  grunt.loadNpmTasks("grunt-htmlhint");
  grunt.loadNpmTasks("grunt-contrib-jshint");
  grunt.loadNpmTasks("grunt-sass");
  grunt.loadNpmTasks("grunt-contrib-concat");
  grunt.loadNpmTasks("grunt-contrib-uglify");
  grunt.loadNpmTasks("grunt-assemble");
  grunt.loadNpmTasks("grunt-contrib-copy");
  grunt.loadNpmTasks("grunt-contrib-watch");
  grunt.loadNpmTasks("grunt-newer");
  grunt.loadNpmTasks("grunt-contrib-clean");
  grunt.loadNpmTasks("grunt-smartrev");

  // custom tasks
  grunt.registerTask("default", ["dev"]);

  grunt.registerTask("test", ["htmlhint", "jshint"]);
  grunt.registerTask("dev", [
    "newer:copy:npmmodules",
    "assemble:dev",
    "assemble:dev",
    "sass:dev",
    "uglify:dev",
    "copy:images",
    "copy:favicon",
    "copy:files",
    "copy:fonts",
  ]);
  grunt.registerTask("build", [
    "dev",
    "test",
    "sass:build",
    "uglify:build",
    "smartrev:build",
    "clean",
  ]);
};
