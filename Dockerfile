# Build app
FROM node:10-alpine as dist

# Set workdir
WORKDIR /app

# Install npm dependencies
COPY package.json package-lock.json ./
RUN npm install

# Copy files and build app
COPY . .
RUN npm run build

# Serve app
FROM nginx:alpine
COPY --from=dist /app/build /usr/share/nginx/html

# EXPOSE 80

# # Set workdir
# WORKDIR /app

# # Install npm dependencies
# COPY package.json package-lock.json ./
# RUN npm install

# # Copy files and build app
# COPY . /app
# RUN npm run build

# # Copy nginx config
# COPY default.conf /etc/nginx/conf.d/

# # Cleanup
# RUN rm -rf node_modules source

# # Define default command.
# CMD ["nginx", "-g", "daemon off;"]
