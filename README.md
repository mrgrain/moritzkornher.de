# moritzkornher.de

Personal portfolio and CV page.

## Set up

Install dependencies via `npm install` and run `grunt dev` for development.

## Building

Use `npm run build` to create a production ready build.

## Docker

Build the docker container with `docker build . -t moritzkornher:latest`

Run the container with `docker run -d -P moritzkornher:latest`
