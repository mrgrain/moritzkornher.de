module.exports.register = function (Handlebars, options) {
  Handlebars.registerHelper("if_even", function (conditional, options) {
    if (conditional % 2 == 0) {
      return options.fn(this);
    } else {
      return options.inverse(this);
    }
  });
  Handlebars.registerHelper("if_odd", function (conditional, options) {
    if (conditional % 2 != 0) {
      return options.fn(this);
    } else {
      return options.inverse(this);
    }
  });
};
