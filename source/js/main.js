$(document).ready(function () {
  // navigate from a source to a target
  function navigate(source, target, noAnimation, noHashUpdate) {
    // hide page scrollbar during transition
    $("body").addClass("hide-scrollbar");

    // disable animations
    if (noAnimation) {
      source.addClass("no-animation");
      target.addClass("no-animation");
    }

    // Add new position class
    if (target.hasClass("is-left")) source.addClass("is-right");
    if (target.hasClass("is-right")) source.addClass("is-left");
    if (target.hasClass("is-beneath")) source.addClass("is-above");
    if (target.hasClass("is-above")) source.addClass("is-beneath");

    // Generic add/remove classes
    source.removeClass("is-in").addClass("is-out");
    target
      .addClass("is-in")
      .removeClass("is-out is-left is-right is-above is-beneath");
    source.find("nav").removeClass("is-fixed");

    // Update navigation
    if (!noHashUpdate) {
      history.replaceState(null, null, "#" + target.attr("id"));
    }

    // Scroll to top
    $("*").scrollTop(0);

    // reenable animations
    if (noAnimation) {
      $("body").removeClass("hide-scrollbar");
      source.removeClass("no-animation");
      target.removeClass("no-animation");
      if (!target.hasClass("is-out")) {
        target.find("nav").addClass("is-fixed");
      }
    }
  }

  // navigate to a slide based on the hash fragment
  function navigateByHash(source, target) {
    const hash = target.attr("id");
    if (hash && 0 === hash.indexOf("project-")) {
      navigate(source, $("#projects").first(), true, true);
      target.get(0).scrollIntoView(true);
    } else {
      navigate(source, target, true);
    }
  }

  // navigate to hash on first pageload
  $(function () {
    var targetHash = window.location.hash.substring(1);
    var target = $("#" + targetHash).first();
    var current = $("section.page.is-in").first();

    if (targetHash && target) {
      navigateByHash(current, target);
    }
  });

  // Navigation Elements
  $("nav a").on("click", function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();

    // Find Elements
    var target = $($(this).attr("href"));
    var source = $(this).parents("section.page").first();

    // Navigate
    navigate(source, target);
  });

  // key navigation
  $(document).on("keyup", function (e) {
    var current = $("section.page.is-in").first();
    var onStart = current.is("#start");
    var toStart = $("#start").first();
    var target = false;

    switch (e.which) {
      case 37: // left
        target = current.is("#mycv")
          ? toStart
          : onStart
          ? $("#profiles").first()
          : false;
        break;
      case 38: // up
        target =
          current.is("#projects") && $(window).scrollTop() === 0
            ? toStart
            : onStart
            ? $("#pay-report").first()
            : false;
        break;
      case 39: // right
        target = current.is("#profiles")
          ? toStart
          : onStart
          ? $("#mycv").first()
          : false;
        break;
      case 40: // down
        target = onStart
          ? $("#projects").first()
          : current.is("#pay-report")
          ? toStart
          : false;
        break;
    }

    console.log(e.target);

    // we have a direction!
    if (current.length && target.length) {
      console.log("trapped!");
      e.preventDefault();
      e.stopImmediatePropagation();
      // Navigate
      navigate(current, target);
    }
  });

  // Scroll navigation
  var inspectScrolling = false;
  var lastScroll = Date.now();
  var timeSinceLastScroll = -1;
  function preventOverScrolling() {
    inspectScrolling = true;
    setTimeout(function () {
      inspectScrolling = false;
    }, 3000);
  }

  $(document).on("scroll", { passive: false }, function (e) {
    timeSinceLastScroll = Date.now() - lastScroll;
    lastScroll = Date.now();
    if (inspectScrolling && timeSinceLastScroll < 50) {
      return;
    }

    // find delta
    var delta;
    if (e.wheelDelta) {
      delta = e.wheelDelta;
    } else {
      delta = -1 * e.deltaY;
    }

    // maybe navigate
    var current = $("section.page.is-in").first();

    // scroll down
    if (delta < 0) {
      if (current.is("#start")) {
        // no need to stop scrolling
        return navigate(current, $("#projects").first());
      }
      if (current.is("#pay-report")) {
        preventOverScrolling();
        return navigate(current, $("#start").first());
      }
    }

    // scroll up
    if ($(window).scrollTop() === 0 && delta > 0) {
      if (current.is("#start")) {
        return navigate(current, $("#pay-report").first());
      }
      if (current.is("#projects")) {
        preventOverScrolling();
        return navigate(current, $("#start").first());
      }
    }
  });

  // Fixed Navigation
  $("section.page").on(
    "transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd",
    function (e) {
      // Only on pages with position transitions
      if (
        this === e.target &&
        0 <= $.inArray(e.propertyName, ["top", "left"])
      ) {
        $("body").removeClass("hide-scrollbar");
        if (!$(this).hasClass("is-out")) {
          $(this).find("nav").addClass("is-fixed");
        }
      }
    }
  );

  // ScrollSpy
  var previousScroll = 0;
  $(window).scroll(function () {
    // Get container scroll position
    var fromTop = $(this).scrollTop(),
      scrollDown;
    scrollDown = fromTop > previousScroll;
    previousScroll = fromTop;

    // CV Header
    if (fromTop > 0) {
      $("#mycv h1").addClass("at-side");
    }
    if (!scrollDown && fromTop < 30) {
      $("#mycv h1").removeClass("at-side");
    }
  });
});
