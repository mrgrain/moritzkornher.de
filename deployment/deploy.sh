#!/usr/bin/env sh
set -e

# Ensure inputs
if [ -z ${1+x} ]; then
    echo "TARGET parameter not passed in";
    exit 1;
fi
if [ -z ${PORTAINER_HOST+x} ]; then
    echo "PORTAINER_HOST is not set";
    exit 1;
fi
if [ -z ${PORTAINER_URL+x} ]; then
    echo "PORTAINER_URL is not set";
    exit 1;
fi
if [ -z ${PORTAINER_STACK+x} ]; then
    echo "PORTAINER_STACK is not set";
    exit 1;
fi
if [ -z ${PORTAINER_ENDPOINT+x} ]; then
    echo "PORTAINER_ENDPOINT is not set";
    exit 1;
fi
if [ -z ${PORTAINER_CI_USER+x} ]; then
    echo "PORTAINER_CI_USER is not set";
    exit 1;
fi
if [ -z ${PORTAINER_CI_PASSWORD+x} ]; then
    echo "PORTAINER_CI_PASSWORD is not set";
    exit 1;
fi

# Display commands
set -x

# Request access
ACCESS_TOKEN=`ssh ${PORTAINER_HOST} 'curl -fsS -X POST '"${PORTAINER_URL}"'/api/auth -H "Content-Type: application/json" -d "{ \"Username\": \"'"${PORTAINER_CI_USER}"'\", \"Password\": \"'"${PORTAINER_CI_PASSWORD}"'\"}"' \
    | jq -r '.jwt'`

# Prepare stack update file
ssh ${PORTAINER_HOST} docker pull mrgrain/moritzkornher.de:build-$BITBUCKET_BUILD_NUMBER
jq -n -c --arg stack "$(cat ./deployment/${1}.yml)" \
   '{StackFileContent: $stack, Env: [], Prune: false}' \
   | sed -e "s/:latest/:build-$BITBUCKET_BUILD_NUMBER/g" \
   | ssh ${PORTAINER_HOST} 'curl -fsS -X PUT '"${PORTAINER_URL}"'/api/stacks/'"${PORTAINER_STACK}"'?endpointId='"${PORTAINER_ENDPOINT}"' -H "Content-Type: application/json" -H "Authorization: Bearer '"${ACCESS_TOKEN}"'" -d @-'
